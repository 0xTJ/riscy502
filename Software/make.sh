#!/bin/sh

cl65 -t none -o riscy502.bin --start-addr 0x1000 -O -m riscy502.map main.asm --cpu 65816
srec_cat riscy502.bin -Binary -offset 0x1000 -Output riscy502.hex -Motorola -address-length=3 -disable=data-count -disable=header -Execution_Start_Address=0x1000   