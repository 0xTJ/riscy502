.p816
.smart

PUT_STR .set $00E04E

.org $1000
__STARTUP__:
.i16
.a8
    lda #$00
    ldx #opcode_invalid_message
    jsl PUT_STR
    rtl

opcode_dispatch:
    sep #$30
    lda instr+0

    ; Convert instr[6:0] into offset in dispatch table
    asl

    ; Jump to pointer at offset in dispatch table
    tax
    jmp (opcode_dispatch_table,x)

opcode_load:


opcode_invalid:
    sep #$20
    rep #$10
    lda .bankbyte(opcode_invalid_message)
    ldx .loword(opcode_invalid_message)
    jsl PUT_STR
    rts

opcode_dispatch_table:
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_load         ; LOAD
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; LOAD-FP
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; custom-0
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; MISC-MEM
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; OP-IMM
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; AUIPC
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; OP-IMM-32
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; 48b
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; STORE
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; STORE-FP
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; custom-1
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; AMO
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; OP
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; LUI
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; OP-32
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; 64b
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; MADD
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; MSUB
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; NMSUB
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; NMADD
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; OP-FP
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; reserved
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; custom-2/rv128
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; 48b
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; BRANCH
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; JALR
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; reserved
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; JAL
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; SYSTEM
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; reserved
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; custom-3/rv128
.repeat 3
.addr   opcode_invalid      ; Compressed instructions
.endrepeat
.addr   opcode_invalid      ; >= 80b

instr:
.dword  0

opcode_invalid_message:
.asciiz "Encountered invalid opcode!"
